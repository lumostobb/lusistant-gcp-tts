package lumos.ttsgcp;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.Locale;

import lumos.ttsgcp.tts.TextToSpeechManger;
import lumos.ttsgcp.tts.gcp.AudioConfig;
import lumos.ttsgcp.tts.gcp.EAudioEncoding;
import lumos.ttsgcp.tts.gcp.GCPTTS;
import lumos.ttsgcp.tts.gcp.ESSMLlVoiceGender;
import lumos.ttsgcp.tts.gcp.GCPTTSAdapter;
import lumos.ttsgcp.tts.gcp.GCPVoice;
import lumos.ttsgcp.tts.gcp.VoiceCollection;
import lumos.ttsgcp.tts.gcp.VoiceList;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = MainActivity.class.getName();
    private static final int TEXT_TO_SPEECH_CODE = 0x100;

    private TextToSpeechManger mTextToSpeechManger;
    private GCPTTS mGCPTTS;

    private EditText mEditText;
    private Button mButtonSpeak;
    private Button mButtonSpeakPause;
    private Button mButtonSpeakStop;
    private Button mButtonRefresh;
    private Handler mHandler;

    private int mPitch;
    private int mSpeakRate;
    private String mPause;
    private String mResume;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buildViews();
        final Context self = this;
        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                EUiHandlerStatus eUiHandlerStatus = EUiHandlerStatus.getStatus(msg.what);
                switch (eUiHandlerStatus) {
                    case SHOW_TOAST: {
                        String errorText = (String) msg.obj;
                        if (errorText != null) {
                            Toast.makeText(self, errorText, Toast.LENGTH_LONG).show();
                        }
                        break;
                    }

                }

            }
        };

        initGCPTTS();
        //initAndroidTTS();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mTextToSpeechManger != null) {
            if (mButtonSpeakPause.getText().toString().compareTo(mPause) == 0) {
                mTextToSpeechManger.resume();
            }
        }
    }

    @Override
    protected void onPause() {
        if (mTextToSpeechManger != null) {
            mTextToSpeechManger.pause();
        }

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (mTextToSpeechManger != null) {
            mTextToSpeechManger.exit();
            mTextToSpeechManger = null;
        }

        if (mGCPTTS != null) {
            mGCPTTS.removeSpeakListener();
            mGCPTTS = null;
        }


        super.onDestroy();
    }

    private void buildViews() {
        mEditText = findViewById(R.id.ettIdText);
        mButtonSpeak = findViewById(R.id.btnIdSpeak);
        mButtonSpeakPause = findViewById(R.id.btnIdPauseAndResume);
        mButtonSpeakStop = findViewById(R.id.btnIdStop);
        mPitch = 2000;
        mSpeakRate = 75;
        mPause = getResources().getString(R.string.btnPtSpeechPause);
        mResume = getResources().getString(R.string.btnPtSpeechResume);

        mButtonSpeak.setOnClickListener(this);
        mButtonSpeakPause.setOnClickListener(this);
        mButtonSpeakStop.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TEXT_TO_SPEECH_CODE:
                if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {

                } else {
                    Toast.makeText(this,
                            "You do not have the text to speech file you have to install",
                            Toast.LENGTH_LONG).show();
                    Intent installIntent = new Intent();
                    installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                    startActivity(installIntent);
                }
                break;
        }
    }

    private void initGCPTTS() {

        final Context selfContext = this;
        VoiceList voiceList = new VoiceList();
        voiceList.addVoiceListener(new VoiceList.IVoiceListener() {
            @Override
            public void onResponse(String text) {
                JsonElement jsonElement = new JsonParser().parse(text);
                if (jsonElement == null || jsonElement.getAsJsonObject() == null ||
                        jsonElement.getAsJsonObject().get("voices").getAsJsonArray() == null) {
                    Log.e(TAG, "get error json");
                    return;
                }

                JsonObject jsonObject = jsonElement.getAsJsonObject();
                JsonArray jsonArray = jsonObject.get("voices").getAsJsonArray();
                final VoiceCollection voiceCollection = new VoiceCollection();
                GCPVoice gcpVoice = new GCPVoice("tr-TR", "tr-TR-Standard-A", ESSMLlVoiceGender.FEMALE, 24000);
                voiceCollection.add("tr-TR", gcpVoice);

                mGCPTTS = new GCPTTS();
                mGCPTTS.addSpeakListener(new GCPTTS.ISpeakListener() {
                    @Override
                    public void onSuccess(String message) {
                        Log.i(TAG, message);
                    }

                    @Override
                    public void onFailure(String errorMessage, String speakMessage) {
                        Message message = mHandler.obtainMessage(EUiHandlerStatus.SHOW_TOAST.ordinal(), errorMessage);
                        message.sendToTarget();

                        Log.e(TAG, "speak fail : " + errorMessage);
                    }
                });
            }

            @Override
            public void onFailure(String error) {
                Message message = mHandler.obtainMessage(EUiHandlerStatus.UPDATE_SPINNER.ordinal(), null);
                message.sendToTarget();

                mGCPTTS = null;
                Log.e(TAG, "Loading Voice List Error, error code : " + error);
            }
        });
        voiceList.start();
    }

    private TextToSpeechManger loadGCPTTS() {
        if (mGCPTTS == null) {
            return null;
        }

        String languageCode = "tr-TR";
        String name = "tr-TR-Standard-A";
        float pitch = ((float) (mPitch - 2000) / 100);
        float speakRate = ((float) (mSpeakRate + 25) / 100);

        GCPVoice gcpVoice = new GCPVoice(languageCode, name);
        AudioConfig audioConfig = new AudioConfig.Builder()
                .addAudioEncoding(EAudioEncoding.MP3)
                .addSpeakingRate(speakRate)
                .addPitch(pitch)
                .build();

        mGCPTTS.setGCPVoice(gcpVoice);
        mGCPTTS.setAudioConfig(audioConfig);
        GCPTTSAdapter gcpttsAdapter = new GCPTTSAdapter(mGCPTTS);

        return new TextToSpeechManger(gcpttsAdapter);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btnIdSpeak: {
                if (mTextToSpeechManger != null) {
                    mTextToSpeechManger.stop();
                }


                mTextToSpeechManger = loadGCPTTS();
                if (mTextToSpeechManger != null) {
                    mTextToSpeechManger.speak(mEditText.getText().toString());
                }

                mButtonSpeakPause.setText(mPause);
                break;
            }

            case R.id.btnIdPauseAndResume: {
                if (mTextToSpeechManger != null) {
                    Button button = findViewById(R.id.btnIdPauseAndResume);
                    if (button.getText().toString().compareTo(mPause) == 0) {
                        mTextToSpeechManger.pause();
                        button.setText(mResume);
                    } else {
                        mTextToSpeechManger.resume();
                        button.setText(mPause);
                    }
                }
                break;
            }
            case R.id.btnIdStop: {
                if (mTextToSpeechManger != null) {
                    mTextToSpeechManger.stop();
                }
            }
        }
    }

    enum EUiHandlerStatus {
        SHOW_TOAST,
        UPDATE_SPINNER,
        NONE;

        static EUiHandlerStatus getStatus(int id) {
            if(id == SHOW_TOAST.ordinal()) {
                return SHOW_TOAST;
            } else if (id == UPDATE_SPINNER.ordinal()) {
                return UPDATE_SPINNER;
            } else {
                return NONE;
            }
        }
    }

}
